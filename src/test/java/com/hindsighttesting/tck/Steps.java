package com.hindsighttesting.tck;

import static org.junit.Assert.assertTrue;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.PendingException;

public class Steps {

    @Given("^the following feature:$")
    public void the_following_feature(String arg1) {

    }

    @When("^Cucumber runs the feature$")
    public void Cucumber_runs_the_feature() {

    }

    @Then("^the feature passes$")
    public void the_feature_passes() {

    }

    @Given("^a scenario with:$")
    public void a_scenario_with(String arg1) {

    }

    @Given("^the step \"([^\"]*)\" has a passing mapping$")
    public void the_step_has_a_passing_mapping(String arg1) {

    }

    @When("^Cucumber executes the scenario$")
    public void Cucumber_executes_the_scenario() {

    }

    @Then("^the scenario passes$")
    public void the_scenario_passes() {

    }

    @Given("^the step \"([^\"]*)\" has a failing mapping$")
    public void the_step_has_a_failing_mapping(String arg1) {

    }

    @Then("^the scenario fails$")
    public void the_scenario_fails() {

    }

    @Then("^the step \"([^\"]*)\" is skipped$")
    public void the_step_is_skipped(String arg1) {

    }

    @Given("^the step \"([^\"]*)\" has a pending mapping$")
    public void the_step_has_a_pending_mapping(String arg1) {

    }

    @Then("^the scenario is pending$")
    public void the_scenario_is_pending() {

    }

    @Then("^the scenario is undefined$")
    public void the_scenario_is_undefined() {

    }

    @When("^Cucumber runs the scenario with steps for a calculator$")
    public void Cucumber_runs_the_scenario_with_steps_for_a_calculator() {

    }

    @Given("^the step \"([^\"]*)\" has a passing mapping that receives a data table$")
    public void the_step_has_a_passing_mapping_that_receives_a_data_table(String arg1) {

    }

    @Then("^the received data table array equals the following:$")
    public void the_received_data_table_array_equals_the_following(String arg1) {

    }

    @Given("^the following data table in a step:$")
    public void the_following_data_table_in_a_step(String arg1) {

    }

    @When("^the data table is passed to a step mapping that converts it to key/value pairs$")
    public void the_data_table_is_passed_to_a_step_mapping_that_converts_it_to_key_value_pairs() {

    }

    @Then("^the data table is converted to the following:$")
    public void the_data_table_is_converted_to_the_following(String arg1) {

    }

    @When("^the data table is passed to a step mapping that gets the row arrays without the header$")
    public void the_data_table_is_passed_to_a_step_mapping_that_gets_the_row_arrays_without_the_header() {

    }

    @Given("^the step \"([^\"]*)\" has a mapping failing with the message \"([^\"]*)\"$")
    public void the_step_has_a_mapping_failing_with_the_message(String arg1, String arg2) {

    }

    @Then("^the failure message \"([^\"]*)\" is output$")
    public void the_failure_message_is_output(String arg1) {

    }

    @Then("^the scenario called \"([^\"]*)\" is reported as failing$")
    public void the_scenario_called_is_reported_as_failing(String arg1) {

    }

    @Then("^the scenario called \"([^\"]*)\" is not reported as failing$")
    public void the_scenario_called_is_not_reported_as_failing(String arg1) {

    }

    @Given("^a passing before hook$")
    public void a_passing_before_hook() {

    }

    @Given("^a passing after hook$")
    public void a_passing_after_hook() {

    }

    @When("^Cucumber executes a scenario$")
    public void Cucumber_executes_a_scenario() {

    }

    @Then("^the before hook is fired before the scenario$")
    public void the_before_hook_is_fired_before_the_scenario() {

    }

    @Then("^the after hook is fired after the scenario$")
    public void the_after_hook_is_fired_after_the_scenario() {

    }

    @Given("^a passing around hook$")
    public void a_passing_around_hook() {

    }

    @Then("^the around hook fires around the scenario$")
    public void the_around_hook_fires_around_the_scenario() {

    }

    @Then("^the around hook is fired around the other hooks$")
    public void the_around_hook_is_fired_around_the_other_hooks() {

    }

    @Given("^a hook tagged with \"([^\"]*)\"$")
    public void a_hook_tagged_with(String arg1) {

    }

    @When("^Cucumber executes a scenario tagged with \"([^\"]*)\"$")
    public void Cucumber_executes_a_scenario_tagged_with(String arg1) {

    }

    @Then("^the hook is fired$")
    public void the_hook_is_fired() {

    }

    @Then("^the hook is not fired$")
    public void the_hook_is_not_fired() {

    }

    @When("^Cucumber executes a scenario with no tags$")
    public void Cucumber_executes_a_scenario_with_no_tags() {

    }

    @Given("^an untagged hook$")
    public void an_untagged_hook() {

    }

    @Given("^a scenario tagged with \"([^\"]*)\"$")
    public void a_scenario_tagged_with(String arg1) {

    }

    @When("^Cucumber executes scenarios tagged with \"([^\"]*)\"$")
    public void Cucumber_executes_scenarios_tagged_with(String arg1) {

    }

    @Then("^only the first scenario is executed$")
    public void only_the_first_scenario_is_executed() {

    }

    @When("^Cucumber executes scenarios not tagged with \"([^\"]*)\"$")
    public void Cucumber_executes_scenarios_not_tagged_with(String arg1) {

    }

    @When("^Cucumber executes scenarios tagged with \"([^\"]*)\" or \"([^\"]*)\"$")
    public void Cucumber_executes_scenarios_tagged_with_or(String arg1, String arg2) {

    }

    @Then("^only the first two scenarios are executed$")
    public void only_the_first_two_scenarios_are_executed() {

    }

    @Given("^a scenario tagged with \"([^\"]*)\" and \"([^\"]*)\"$")
    public void a_scenario_tagged_with_and(String arg1, String arg2) {

    }

    @When("^Cucumber executes scenarios tagged with both \"([^\"]*)\" and \"([^\"]*)\"$")
    public void Cucumber_executes_scenarios_tagged_with_both_and(String arg1, String arg2) {

    }

    @When("^Cucumber executes scenarios not tagged with \"([^\"]*)\" nor \"([^\"]*)\"$")
    public void Cucumber_executes_scenarios_not_tagged_with_nor(String arg1, String arg2) {

    }

    @Then("^only the third scenario is executed$")
    public void only_the_third_scenario_is_executed() {

    }

    @When("^Cucumber executes scenarios not tagged with both \"([^\"]*)\" and \"([^\"]*)\"$")
    public void Cucumber_executes_scenarios_not_tagged_with_both_and(String arg1, String arg2) {

    }

    @Then("^only the second, third and fourth scenarios are executed$")
    public void only_the_second_third_and_fourth_scenarios_are_executed() {

    }

    @When("^Cucumber executes scenarios tagged with \"([^\"]*)\" or without \"([^\"]*)\"$")
    public void Cucumber_executes_scenarios_tagged_with_or_without(String arg1, String arg2) {

    }

    @Given("^a scenario tagged with \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\"$")
    public void a_scenario_tagged_with_and(String arg1, String arg2, String arg3) {

    }

    @When("^Cucumber executes scenarios tagged with \"([^\"]*)\" but not with both \"([^\"]*)\" and \"([^\"]*)\"$")
    public void Cucumber_executes_scenarios_tagged_with_but_not_with_both_and(String arg1, String arg2, String arg3) {

    }

    @Given("^a feature tagged with \"([^\"]*)\"$")
    public void a_feature_tagged_with(String arg1) {

    }

    @Given("^a scenario without any tags$")
    public void a_scenario_without_any_tags() {

    }

    @Then("^the scenario is executed$")
    public void the_scenario_is_executed() {

    }

    @Given("^the step \"([^\"]*)\" has no mapping$")
    public void the_step_has_no_mapping(String arg1) {

    }

    @Then("^a \"([^\"]*)\" step definition snippet for /^I am a veggie\\$/ is suggested$")
    public void a_step_definition_snippet_for_I_am_a_veggie$_is_suggested(String arg1) {

    }

    @Then("^a \"([^\"]*)\" step definition snippet for /^I eat meat\\$/ is suggested$")
    public void a_step_definition_snippet_for_I_eat_meat$_is_suggested(String arg1) {

    }

    @Then("^a \"([^\"]*)\" step definition snippet for /^I get sick\\$/ is suggested$")
    public void a_step_definition_snippet_for_I_get_sick$_is_suggested(String arg1) {
        assertTrue(false);
    }

    @Given("^the steps have no mappings$")
    public void the_steps_have_no_mappings() {

    }

    @Then("^a \"([^\"]*)\" step definition snippet for /^I love food\\$/ is suggested$")
    public void a_step_definition_snippet_for_I_love_food$_is_suggested(String arg1) {

    }

    @Then("^a \"([^\"]*)\" step definition snippet for /^I eat gherkins\\$/ is suggested$")
    public void a_step_definition_snippet_for_I_eat_gherkins$_is_suggested(String arg1) {

    }

    @Then("^a \"([^\"]*)\" step definition snippet for /^I eat cucumbers\\$/ is suggested$")
    public void a_step_definition_snippet_for_I_eat_cucumbers$_is_suggested(String arg1) {

    }

    @Then("^a \"([^\"]*)\" step definition snippet for /^I feel satiated\\$/ is suggested$")
    public void a_step_definition_snippet_for_I_feel_satiated$_is_suggested(String arg1) {

    }

    @Then("^a \"([^\"]*)\" step definition snippet for /^I feel energized\\$/ is suggested$")
    public void a_step_definition_snippet_for_I_feel_energized$_is_suggested(String arg1) {
        assertTrue(false);
    }

    @Then("^a \"([^\"]*)\" step definition snippet for /^I have nothing left to eat\\$/ is suggested$")
    public void a_step_definition_snippet_for_I_have_nothing_left_to_eat$_is_suggested(String arg1) {

    }

    @Then("^a \"([^\"]*)\" step definition snippet for /^the following Wikipedia excerpt:\\$/ with a doc string is suggested$")
    public void a_step_definition_snippet_for_the_following_Wikipedia_excerpt_$_with_a_doc_string_is_suggested(
            String arg1) {
        assertTrue(false);
    }

    @Then("^a \"([^\"]*)\" step definition snippet for /^the following cucumbers:\\$/ with a data table is suggested$")
    public void a_step_definition_snippet_for_the_following_cucumbers_$_with_a_data_table_is_suggested(String arg1) {

    }

    @Given("^a World variable initialized to (\\d+)$")
    public void a_World_variable_initialized_to(int arg1) {

    }

    @When("^Cucumber executes a scenario that increments the World variable by (\\d+)$")
    public void Cucumber_executes_a_scenario_that_increments_the_World_variable_by(int arg1) {

    }

    @Then("^the World variable should have contained (\\d+) at the end of the scenario$")
    public void the_World_variable_should_have_contained_at_the_end_of_the_scenario(int arg1) {

    }

    @When("^Cucumber executes two scenarios that increment the World variable by (\\d+)$")
    public void Cucumber_executes_two_scenarios_that_increment_the_World_variable_by(int arg1) {

    }

    @Then("^the World variable should have contained (\\d+) at the end of the first scenario$")
    public void the_World_variable_should_have_contained_at_the_end_of_the_first_scenario(int arg1) {
        // Express the Regexp above with the code you wish you had
        throw new PendingException();
    }

    @Then("^the World variable should have contained (\\d+) at the end of the second scenario$")
    public void the_World_variable_should_have_contained_at_the_end_of_the_second_scenario(int arg1) {

    }

    @Given("^a World function$")
    public void a_World_function() {
        throw new RuntimeException("Fail me");
    }

    @When("^Cucumber executes a scenario that calls the World function$")
    public void Cucumber_executes_a_scenario_that_calls_the_World_function() {

    }

    @Then("^the World function should have been called$")
    public void the_World_function_should_have_been_called() {

    }

    @Given("^a custom World constructor$")
    public void a_custom_World_constructor() {

    }
}
