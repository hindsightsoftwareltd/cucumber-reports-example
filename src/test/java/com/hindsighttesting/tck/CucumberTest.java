package com.hindsighttesting.tck;

import org.junit.runner.RunWith;

import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@Cucumber.Options(format = { "json:target/cucumber.json" }, features = { "cucumber-tck" })
public class CucumberTest {

}
